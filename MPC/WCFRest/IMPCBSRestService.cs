﻿using PCM.MPC.WCFRest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCM.MPC.WCFRest
{
    public interface IMPCBSRestService
    {
        /// <summary>
        /// "Die für den Mandanten gelisteten Lieferanten werden abgerufen. " from the MPC Documentation.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="sessionGUID"></param>
        /// <returns></returns>
        Task<ArrayOfDelivery> EklDeliveryGetItems(int mid, string sessionGUID);

        /// <summary>
        /// "Die für den Mandanten vorhandenen Einkaufslisten werden mit ihren Basisattributen abgerufen." from the MPC Documentation.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="sessionGUID"></param>
        /// <returns></returns>
        Task<ArrayOfEkl> EklGetItems(int mid, string sessionGUID);

        /// <summary>
        /// "Die für den Mandanten vorhandenen Einkaufslisten-Positionen werden mit ihren Attributen abgerufen."
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="eklid"></param>
        /// <param name="sessionGUID"></param>
        /// <returns></returns>
        Task<ArrayOfEklPosition> EklPositionGetItems(int mid, int eklid, string sessionGUID);

        /// <summary>
        /// "Die bestehenden Positionen der Einkaufsliste werden mit der übertragenen Liste
        /// abgeglichen.Der Client muss im Falle eines Updates die Positionsnummer von neuen
        /// Positionen verwalten.Zu löschende Positionen bekommen eine -1 in der Positionsnummer
        /// gesetzt."
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="sessionGUID"></param>
        /// <param name="updateItems"></param>
        /// <returns></returns>
        Task<Response> EklPositionUpdateItems(int mid, string sessionGUID, ArrayOfEklPosition updateItems);

        /// <summary>
        /// Request list of LIDArticles which matches the given article number.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="articleNumber"></param>
        /// <param name="sessionGUID"></param>
        /// <returns></returns>
        Task<ArrayOfLIDArticle> LIDArticleGetItemByArticleNumber(int mid, int articleNumber, string sessionGUID);

        /// <summary>
        /// Request list of LIDArticles which matches the given lid.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="lid"></param>
        /// <param name="sessionGUID"></param>
        /// <returns></returns>
        Task<ArrayOfLIDArticle> LIDArticleGetItemByLId(int mid, int lid, string sessionGUID);

        /// <summary>
        /// Request list of LIDarticles which matches the given description.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="description"></param>
        /// <param name="sessionGUID"></param>
        /// <returns></returns>
        Task<ArrayOfLIDArticle> LIDArticleGetItemByDescription(int mid, string description, string sessionGUID);
    }
}
