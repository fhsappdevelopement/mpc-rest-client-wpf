﻿using PCM.MPC.WCFRest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCM.MPC.WCFRest
{
    public interface IMPCProdRestService
    {
        /// <summary>
        /// "Die zweistufige Hierarchiestruktur für die Rezepte wird übertragen." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfRzGrpHaupt> RzHierarchieGetItems(int mid, string sessionGUID);


        /// <summary>
        /// "Die Rezepte mit Rezeptpositionen des Mandanten werden abgefragt." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfRz> RzGetItems(int mid, string sessionGUID);

        /// <summary>
        /// "Ein bestimmtes Rezept mit Rezeptpositionen des Mandanten wird abgerufen." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="rzid">Rezept Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns>Rezept</returns>
        Task<Rz> RzGetItemById(int mid, int rzid, string sessionGUID);

        /// <summary>
        /// "Alle Rezeptpositionen zu einem bestimmten Rezept des Mandanten wird abgefragt." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="rzid">Rezept Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns>Rezept</returns>
        Task<ArrayOfRzPos> RzPosGetItemById(int mid, int rzid, string sessionGUID);

        /// <summary>
        /// Create; es wird eine neue Rezeptposition angelegt
        /// </summary>
        /// <param name="rzPos">object to create</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<RzPos> RzPosCreate(RzPos rzPos, int mid, string sessionGUID);

        /// <summary>
        /// Read es wird die Rezeptposition gelesen, zu der eine RZPosID angegeben wurde
        /// </summary>
        /// <param name="rzPos">object to read</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<RzPos> RzPosRead(RzPos rzPos, int mid, string sessionGUID);

        /// <summary>
        /// Update die bestehende Rezeptposition wird unter der RZPosID überschrieben
        /// </summary>
        /// <param name="rzPos">Object to update</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<RzPos> RzPosUpdate(RzPos rzPos, int mid, string sessionGUID);

        /// <summary>
        /// Delete die angegebene Rezeptposition wird gelöscht
        /// </summary>
        /// <param name="rzPos">object to delete</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<RzPos> RzPosDelete(RzPos rzPos, int mid, string sessionGUID);

        /// <summary>
        /// "Alle Menüs mit Menüpositionen des Mandanten werden abgerufen." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfMnu> MnuGetItems(int mid, string sessionGUID);

        /// <summary>
        /// "Ein bestimmtes Menü mit Menüpositionen des Mandanten wird abgerufen." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfMnu> MnuGetItemById(int mid, int mnuId, string sessionGUID);

        /// <summary>
        /// "Alle Positionen zu einem Menü werden abgefragt." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="mnuID">Menu ID</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfMnuPos> MnuPosGetItemById(int mid, int mnuID, string sessionGUID);

        /// <summary>
        /// Create; es wird eine neue Menüposition angelegt
        /// </summary>
        /// <param name="mnuPos">MnuPos Object to created</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<MnuPos> MnuPosCreate(MnuPos mnuPos, int mid, string sessionGUID);

        /// <summary>
        /// "Read; es wird die Menüposition gelesen, zu der eine MnuPosID angegeben wurde"  from the MPC Documentation.
        /// </summary>
        /// <param name="mnuPos">MnuPos Object to read</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<MnuPos> MnuPosRead(MnuPos mnuPos, int mid, string sessionGUID);

        /// <summary>
        /// "Update; die bestehende Menüposition wird unter der MnuPosID überschrieben" from the MPC Documentation.
        /// </summary>
        /// <param name="mnuPos">MnuPos Object to update</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<MnuPos> MnuPosUpdate(MnuPos mnuPos, int mid, string sessionGUID);

        /// <summary>
        /// "Delete; die angegebene Menüposition wird gelöscht" from the MPC Documentation.
        /// </summary>
        /// <param name="mnuPos">MnuPos Object to delete</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<MnuPos> MnuPosDelete(MnuPos mnuPos, int mid, string sessionGUID);

        /// <summary>
        /// "Alle Tagespläne mit Tagesplanpositionen des Mandanten werden abgerufen." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfTp> TpGetItems(int mid, string sessionGUID);

        /// <summary>
        /// "Ein bestimmter Tagesplan mit Tagesplanpositionen des Mandanten wird abgerufen" from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="tpid">Tagesplan-ID</param>
        /// <param name="sessíonGUID"></param>
        /// <returns></returns>
        Task<ArrayOfTp> TpGetItemById(int mid, int tpid, string sessionGUID);

        /// <summary>
        /// "Alle Tagesplanpositionen zu einem bestimmten Tagesplan des Mandanten werden abgefragt." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="tpid">Tagesplan-ID</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfTpPos> TpPosGetItemById(int mid, int tpid, string sessionGUID);

        /// <summary>
        /// "Create; es wird eine neue Tagesplanposition angelegt" from the MPC Documentation.
        /// </summary>
        /// <param name="tpPos">TpPos object to create</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<TpPos> TpPosCreate(TpPos tpPos, int mid, string sessionGUID);

        /// <summary>
        /// "Read; es wird die Tagesplanposition gelesen, zu der eine TpPosID angegeben wurde" from the MPC Documentation.
        /// </summary>
        /// <param name="tpPos">TpPos object to read</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<TpPos> TpPosRead(TpPos tpPos, int mid, string sessionGUID);

        /// <summary>
        /// "Update; die bestehende Tagesplanposition wird unter der TpPosID überschrieben" from the MPC Documentation.
        /// </summary>
        /// <param name="tpPos">TpPos object to update</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<TpPos> TpPosUpdate(TpPos tpPos, int mid, string sessionGUID);

        /// <summary>
        /// "Delete; die angegebene Tagesplanposition wird gelöscht" from the MPC Documentation.
        /// </summary>
        /// <param name="tpPos">TpPos object to delete</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<TpPos> TpPosDelete(TpPos tpPos, int mid, string sessionGUID);

        /// <summary>
        /// "Alle Wochenpläne mit Wochenplanpositionen des Mandanten werden abgerufen." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfWp> WpGetItems(int mid, string sessionGUID);

        /// <summary>
        /// "Ein bestimmter Wochenplan mit Wochenplanpositionen des Mandanten wird abgerufen." from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="wpid">Wochenplan-ID</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<ArrayOfWp> WpGetItemById(int mid, int wpid, string sessionGUID);

        /// <summary>
        /// "Alle Wochenplanpositionen zu einem bestimmten Wochenplan des Mandanten werden abgefragt" from the MPC Documentation.
        /// </summary>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="wpid">Wochenplan-ID</param>
        /// <param name="sesionGUID"></param>
        /// <returns></returns>
        Task<ArrayOfWpPos> WpPosGetItemById(int mid, int wpid, string sessionGUID);

        /// <summary>
        /// "Create; es wird eine neue Wochenplanposition angelegt" from the MPC Documentation.
        /// </summary>
        /// <param name="wpPos">WpPos object to create</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<WpPos> WpPosCreate(WpPos wpPos, int mid, string sessionGUID);

        /// <summary>
        /// "Read; es wird die Wochenplanposition gelesen, zu der eine WpPosID Angegeben wurde" from the MPC Documentation.
        /// </summary>
        /// <param name="wpPos">WpPos object to read</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<WpPos> WpPosRead(WpPos wpPos, int mid, string sessionGUID);

        /// <summary>
        /// "Update; die bestehende Wochenplanposition wird unter der WpPosID überschrieben" from the MPC Documentation.
        /// </summary>
        /// <param name="wpPos">WpPos object to update</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<WpPos> WpPosUpdate(WpPos wpPos, int mid, string sessionGUID);

        /// <summary>
        /// "Delete; die angegebene Wochenplanposition wird gelöscht" from the MPC Documentation. 
        /// </summary>
        /// <param name="wpPos">WpPos object to delete</param>
        /// <param name="mid">Mandanten Id</param>
        /// <param name="sessionGUID">Session Globally Unique IDentifier</param>
        /// <returns></returns>
        Task<WpPos> WpPosDelete(WpPos wpPos, int mid, string sessionGUID);
    }
}
