﻿using PCM.REST.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCM.MPC.WCFRest
{
    public class PCMRestServiceBuilder
    {
        protected RestClient restClient;

        public PCMRestServiceBuilder(string webserviceUri) : this(new Uri(webserviceUri))
        {
        }

        public PCMRestServiceBuilder(Uri webserviceUri)
        {
            this.restClient = new RestClient(webserviceUri);
        }

        public IMPCRestService build()
        {
            return new MPCRestService(this.restClient, new Serializer.MPCXmlSerializer());
        }


        public IMPCInfoRestService buildInfoService()
        {
            return this.build();
        }

        public IMPCBaseRestService buildBaseService()
        {
            return this.build();
        }

        public IMPCInfoBaseRestService buildInfoBaseService()
        {
            return this.build();
        }

        public IMPCBSRestService buildBSService()
        {
            return this.build();
        }

        public IMPCProdRestService buildProdService()
        {
            return this.build();
        }
    }
}
