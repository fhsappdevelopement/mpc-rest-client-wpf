﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    /// <summary>
    /// MPC Webservice Protocol class. See the PCM documentation
    /// for more information.
    /// </summary>
    public class TalkMessage
    {
        [XmlElement("MPC_DSID")]
        public int DataRowID { get; set; }

        [XmlElement("MPC_PSID")]
        public int MPC_PSID { get; set; }

        [XmlElement("PCM_PSID")]
        public int PCM_PSID { get; set; }

        [XmlElement("MessageBody")]
        public string MessageBody { get; set; }

        [XmlElement("MessageDirection")]
        public int MessageDirection { get; set; }

        [XmlElement("ImageBody")]
        public string ImageBody { get; set; }

        [XmlElement("ImageTyp")]
        public string ImageType { get; set; }

        [XmlElement("DateSend")]
        public DateTime DateSend { get; set; }


        /// <summary>
        /// Default constructor.
        /// </summary>
        public TalkMessage()
        {
        }

        /// <summary>
        /// Constructor for sending message.
        /// </summary>
        /// <param name="mpcPsID">MPC PSID (App user).</param>
        /// <param name="pcmPsID">PCM PSID (Company user).</param>
        /// <param name="messageBody">Message.</param>
        public TalkMessage(int mpcPsID, int pcmPsID, string messageBody)
            : this(mpcPsID, pcmPsID, messageBody,  null, null)
        {

        }

        /// <summary>
        /// Constructor for sending message with image.
        /// </summary>
        /// <param name="mpcPsID">MPC PSID (App user).</param>
        /// <param name="pcmPsID">PCM PSID (Company user).</param>
        /// <param name="messageBody">Message.</param>
        /// <param name="imageBody">Image.</param>
        /// <param name="imageTyp">Image Type.</param>
        public TalkMessage(int mpcPsID, int pcmPsID, string messageBody, string imageBody, string imageTyp)
        {
            this.MPC_PSID = mpcPsID;
            this.PCM_PSID = pcmPsID;
            this.MessageBody = messageBody;
            this.ImageBody = imageBody;
            this.ImageType = imageTyp;
        }
    }
}
