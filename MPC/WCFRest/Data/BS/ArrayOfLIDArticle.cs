﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfLIDArticle", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfLIDArticle
    {
        [XmlElement("LIDArticle")]
        public LIDArticle[] list { get; set; }
    }
}
