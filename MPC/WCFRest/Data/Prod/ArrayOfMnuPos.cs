﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfMnuPos", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfMnuPos
    {
        [XmlElement("MnuPos")]
        public MnuPos[] list { get; set; }
    }
}
