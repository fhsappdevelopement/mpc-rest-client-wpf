﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("MnuPos", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class MnuPos
    {
        [XmlElement("MnuPosID")]
        // ID der Menüposition
        public int MnuPosID { get; set; }

        [XmlElement("MnuID")]
        // Verweis auf das Menü zu dem die Position gehört
        public int MnuID { get; set; }

        [XmlElement("ArtID")]
        // Verweis auf die ID des Artikels, wenn die Position
        public int ArtID { get; set; }

        [XmlElement("LID")]
        // ID des Lieferanten, der den Artikel des Menüs liefert
        public int LID { get; set; }

        [XmlElement("BezSp")]
        // Bezeichnung des Menüs im Speiseplan
        public string BezSp { get; set; }

        [XmlElement("RzID")]
        //ID des Rezeptes, falls die Position ein Rezept ist
        public int RzID { get; set; }

        [XmlElement("PosArt")]
        // Positionsart 1 - Artikel 2 - Rezept
        public int PosArt { get; set; }


    }
}