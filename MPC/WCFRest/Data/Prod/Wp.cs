﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("Wp", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class Wp
    {
        [XmlElement("WpID")]
        // ID des Wochenplans
        public int WpID { get; set; }

        [XmlElement("MID")]
        // ID des Mandanten
        public int MID { get; set; }

        [XmlElement("Bez")]
        // Bezeichnung des Wochenplans
        public string Bez { get; set; }

        [XmlArray("WpPosList"), XmlArrayItem("WpPos", typeof(WpPos))]
        // Wochenplan-Positionen
        public WpPos[] WpPosList { get; set; }
    }
}
