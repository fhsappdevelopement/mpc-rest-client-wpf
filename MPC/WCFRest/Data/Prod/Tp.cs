﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("Tp", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class Tp
    {
        [XmlElement("TpID")]
        // ID des Tagesplans
        public int TpID { get; set; }

        [XmlElement("MID")]
        // ID des Mandanten
        public int MID { get; set; }

        [XmlElement("Bez")]
        // Bezeichnung des Tagesplans
        public string Bez { get; set; }

        [XmlElement("Tag")]
        // Tag für den der Plan bestimmt ist 1-Montag, 2-Dienstag, .. 7-Sonntag
        public double Tag { get; set; }

        [XmlArray("TpPosList"), XmlArrayItem("TpPos", typeof(TpPos))]
        // Tagesplan-Positionen
        public TpPos[] TpPosList { get; set; }
    }
}
