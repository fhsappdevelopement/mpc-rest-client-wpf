﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfRzPos", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfRzPos
    {
        [XmlElement("RzPos")]
        public RzPos[] list { get; set; }
    }
}
