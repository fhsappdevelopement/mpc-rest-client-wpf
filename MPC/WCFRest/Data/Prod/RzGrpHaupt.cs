﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{

    [XmlRoot("RzGrpHaupt", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class RzGrpHaupt
    {
        [XmlElement("RzGrpHauptID")]
        public int RzGrpHauptID { get; set; }

        [XmlElement("MID")]
        public int MID { get; set; }

        [XmlElement("Bem")]
        public string Bem { get; set; }

        [XmlElement("Bez")]
        public string Bez { get; set; }

        [XmlArray("RzGrpList"), XmlArrayItem("RzGrp", typeof(RzGrp))]
        public RzGrp[] RzGrpList { get; set; }
     
    }
}
