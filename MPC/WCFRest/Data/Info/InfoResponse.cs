﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    /// <summary>
    /// MPC Webservice Protocol class. See the PCM documentation
    /// for more information.
    /// </summary>
    [XmlRoot("Inforesponse")]
    public class InfoResponse
    {
        [XmlElement("SessionGUID")]
        public string SessionGUID { get; set; }

        [XmlElement("MID")]
        public int MID { get; set; }

        [XmlElement("MPC_PSID")]
        public int MpcPsID { get; set; }

        [XmlElement("ErrorText")]
        public string ErrorText { get; set; }

        [XmlElement("Result")]
        public string Result { get; set; }
    }
}
