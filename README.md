# MPC RestService Client (WPF Version)
Wir stellen hier den MPC RestService Client zur Verfügung, der den Zugriff auf den PCM Rest-Webservice ermöglicht.
Der Client versucht den Service 1:1 in C# abzubilden. D.h. das alle Bennenungen der Bennenung von PCM folgen. Dadurch kann das entsprechende Handbuch von PCM zur Hilfe genommen werden.

**Es handelt sich hier um die WPF Version! Diese wurde leicht für WPF Anwendungen modifiziert und ist nicht ausgiebig auf dieser Plattform getestet worden! (coming soon)**

Mit dem Client können asynchrone Anfragen an den PCM Webservice gestellt werden. D.h. man sollte sich zunächst mit der [asynchronen Programmierung](https://msdn.microsoft.com/de-de/library/hh191443.aspx) in C# außeinandersetzen. Auf jeden Methodenauruf des Client sollte mit `await` gewartet oder alternativ mit dem `Task<T>` Objekt (siehe [Task Parallelism TPL](https://msdn.microsoft.com/de-de/library/dd537609(v=vs.110).aspx)) gearbeitet werden. Letzteres ist nicht zu empfehlen und auch unnötig komplex. Weiter unten sind einige Beispiele mit `await` aufgeführt. Methodenen in denen `await` verwendet wird, müssen `async` deklariert werden (siehe [Asynchrone Programmierung mit Async und Await](https://msdn.microsoft.com/de-de/library/hh191443.aspx)).

1. [MPC RestService-Client zu einem bestehendem Visual Studio 2015 Projekt hinzufügen](#markdown-header-1-mpc-restservice-client-zu-einem-bestehendem-visual-studio-2015-projekt-hinzufugen)
2. [REST Service Übersicht- und Initialisierung](#markdown-header-2-rest-service-ubersicht-und-initialisierung)
3. [Info Modul Klassen (Authentifizierung, Images)](#markdown-header-3-info-modul-klassen-authentifizierung-images)
4. [Base Modul Klassen (Chat)](#markdown-header-4-base-modul-klassen-chat)
5. [BS Modul Klassen (Einkaufslisten, Lieferant)](#markdown-header-5-bs-modul-klassen-einkaufslisten-lieferant)
6. [Prod Modul Klassen (Menüs, Wochenpläne, Tagespläne, Rezepte, Menükompositionen, etc.)](#markdown-header-6-prod-modul-klassen-menus-wochenplane-tagesplane-rezepte-menukompositionen-etc)
7. [Beispiele](#markdown-header-7-beispiele)

**Bei Fragen und Problemen einfach melden!**

## 1 MPC RestService-Client zu einem bestehendem Visual Studio 2015 Projekt hinzufügen
1. Das Projekt klonen oder als *.zip downloaden (und ggf. entpacken). 
2. In Visual Studio 2015 im Fenster "Projektmappen-Explorer (oder im Menü oben "Projekt")" Rechtsklick auf die *"Projektmappe" > "Hinzufügen" > "Vorhandenes Projekt"* und die Projektdatei `PCMRestServiceClient.csproj`auswählen. 
3. Danach in Visual Studio 2015 im Fenster "Projektmappen-Explorer" (oder im Menü oben "Projekt") Rechtsklick auf *"Verweise" > "Verweis hinzufügen" > "Projekte" > "Projektmappe"* und `PCMRestServiceClient` auswählen.
4. Ggf. "Projekt bereinigen" und "Projekt bereitstellen" ausführen (im Menü oben "Erstellen" oder per Rechtsklick auf "Projektmappe")

Die weiter unten dargestellten Klassen sollten dann im Projekt zur Verfügung stehen. Sollte es ein Update geben, einfach das PCMRestServiceClient-Projekt entfernen, Repository wieder clonen (oder pullen) oder einfach per *.zip downloaden, die Dateien austauschen und Schritt 1-4 wiederholen. Bei kleineren Updates (also wenn keine Dateien umbenannt oder neue hinzugefügt werden), reicht auch das Austauschen der Dateien und nur Schritt 4. 

## 2 REST Service Übersicht- und Initialisierung
![IMPC Interface Overview](uml/ClassDiagram-IMPCInterfaces.uml.jpg)

Der REST Service ist durch PCM in mehrere "Module" aufgeteilt. Daher ist die Aufteilung auf unterschiedliche Interfaces abgebildet worden. Um die Nutzung zu vereinfachen, erbt das `IMPCRestService` Interface von allen anderen Interfaces und dieses wird durch die `MPCRestService`-Klasse implementiert. 

Um eine neue Instanz des Webservices zu erstellen, kann der mitgeliferte Builder (Builder-Pattern) verwendet werden:
```
var restService = new PCMRestServiceBuilder(WEBSERVICE_URI).build();
```

**Die URL zum Webservice bitte aus dem PCM Handbuch entnehmen.
Die jeweiligen Methoden bitte aus der Grafik mit den Schnittstellen entnehmen. Die Bennenungen und die Parameterreihenfolge orientieren sich alle an dem Webservice.**

### Infos für "Fortgeschrittene" / dyn. Poly.
Wer eine saubere Trennung der Module innerhalb seines Business Codes wünscht, kann dazu auch die anderen Methodes des Builders verwenden:
```
IMPCInfoRestService restService = new PCMRestServiceBuilder(WEBSERVICE_URI).build();
IMPCBaseRestService restService = new PCMRestServiceBuilder(WEBSERVICE_URI).buildBaseService();
IMPCInfoBaseRestService restService = new PCMRestServiceBuilder(WEBSERVICE_URI).buildInfoBaseService();
IMPCBSRestService restService = new PCMRestServiceBuilder(WEBSERVICE_URI).buildBSService();
IMPCProdRestService restService = new PCMRestServiceBuilder(WEBSERVICE_URI).buildProdService();
```

Anmerkung: Dank explizites bzw. implizites casting kann auch direkt immer die build() Methode verwendet werden. Möchte man aber die Kurzfassung der Variablen Deklaration benutzen (var) und den Datentyp forcieren, können die entsprechenden Extramethoden verwendet (oder gecastet - wer es denn will) werden.

## 3 Info Modul Klassen (Authentifizierung, Images)
![Info Module](uml/ClassDiagram-Info.uml.jpg)

### Beispiel Authentifizierung
```
// maybe show login spinner ..

try
{
    // throws exception if login failed
    var credentials = new MPCCredentials("User", Base64Converter.toUrlBase64("password"), 11111);
    var infoResponse = await restService.InfoGetItem(credentials);
    if (infoResponse.SessionGUID == null || infoResponse.SessionGUID.Length == 0)
    {
        throw new Exception("SessionGUID not available.");
    }
    // maybe navigate to another page here
}
catch (Exception authenticationException) // or catch only MPCRestServiceException
{
    // maybe show error message .. authenticationException.Message
}

// maybe hide login spinner ..
```
*Anmerkung: Die Umsetzung des Info-Modules ist schon etwas älter (aus alten Projekt übernommen) und die `MPCCredentials`-Klasse ein altes Überbleibsel. Der erste Implementierungsansatz war, die Parameter des Services konstant zu kapseln. Letztendlich hat man sich bei dem vorhergehendem App Projekt dazu entschieden, ein weitere Abstraktionsebene einzuschieben und nicht direkt mit den Klassen vom Client in der App zu arbeiten. Auf die Art und Weise war eine saubere Trennung zwischen der Kommunikation mit dem Webservice, dem Business Code und der GUI-Logik möglich.*

## 4 Base Modul Klassen (Chat)
![Base Module](uml/ClassDiagram-Base.uml.jpg)

## 5 BS Modul Klassen (Einkaufslisten, Lieferant)
![BS Module](uml/ClassDiagram-BS.uml.jpg)

## 6 Prod Modul Klassen (Menüs, Wochenpläne, Tagespläne, Rezepte, Menükompositionen, etc.)
![Prod Module](uml/ClassDiagram-Prod.uml.jpg)

## 7 Beispiele
Anmerkung: Die Variablen `sessionGUID` und `MID` kommen aus der `InfoResponse`-Klasse. Diese Werte liegen erst nach einer Authentifizierung vor.
Die Beispieldaten sind aus der PCM Dokumentation entnommen.

### BS Module
**PCM Doc:** 5.1.1 EklLieferant GetItems 
```
ArrayOfDelivery eklDelivery = await this.restService.EklDeliveryGetItems(MID, SessionGUID);
```

**PCM Doc:**5.2.1 Ekl GetItems
```
ArrayOfEkl eklList = await this.restService.EklGetItems(MID, SessionGUID);
```

**PCM Doc:** 5.3.1 EklPosition GetItems
```
ArrayOfEklPosition eklPositions = await this.restService.EklPositionGetItems(MID, 72042, SessionGUID);
```

**PCM Doc:** 5.4.1.1 LIDArticle GetItembyArtNr
```
ArrayOfLIDArticle lidByArtNr = await this.restService.LIDArticleGetItemByArticleNumber(MID, 4711, SessionGUID);
```

**PCM Doc:** 5.4.1.2 LIDArticle GetItembyLID
```
ArrayOfLIDArticle lidByLID = await this.restService.LIDArticleGetItemByLId(MID, 853, SessionGUID);
```

**PCM Doc:** 5.4.1.3 LIDArticle GetItembyBezPCM
```
ArrayOfLIDArticle lidByPCM = await this.restService.LIDArticleGetItemByDescription(MID, "Steak", SessionGUID);
```


### Prod Module
**PCM Doc:** 7.1.1 RzHierarchie GetITems Request
```
ArrayOfRzGrpHaupt grp = await this.restService.RzHierarchieGetItems(MID, SessionGUID);
```

**PCM Doc:** 7.2.1 Rz GetItems Request
```
ArrayOfRz rzs = await this.restService.RzGetItems(MID, SessionGUID);
```

**PCM Doc:** 7.3.1 RzPos
```
ArrayOfRzPos arrRzPosMatchedByID = await this.restService.RzPosGetItemById(MID, 140473, SessionGUID);
```

**PCM Doc:** 7.4 RzPos CRUD
```
RzPos returnPos = await this.restService.RzPosUpdate(rzPosObject, MID, SessionGUID);
```

**PCM Doc:** 7.5.1 Mnu GetItems
```
ArrayOfMnu arrMnu = await this.restService.MnuGetItems(MID, SessionGUID);
```

**PCM Doc:** 7.5.2 Mnu GetItemByID
```
ArrayOfMnu mnuByID = await this.restService.MnuGetItemById(MID, 25438, SessionGUID);
```

**PCM Doc:** 7.6.1 MnuPos GetItembyID
```
ArrayOfMnuPos arrMnuPos = await this.restService.MnuPosGetItemById(MID, 25438, SessionGUID);
```

**PCM Doc:** 7.7 MnuPos CRUD
```
MnuPos mnuPosReturn = await this.restService.MnuPosUpdate(mnuPosObject, MID, SessionGUID);
```

**PCM Doc:** 7.8.1 Tp GetItems 
```
ArrayOfTp arrofTp = await this.restService.TpGetItems(MID, SessionGUID);
```

**PCM Doc:** 7.8.2 Tp GetItembyID
```
ArrayOfTp arrtpByID = await this.restService.TpGetItemById(MID, 57, SessionGUID);
```

**PCM Doc:** 7.9.1 TpPos GetItemById
```
ArrayOfTpPos arrOfTpByID = await this.restService.TpPosGetItemById(MID, 57, SessionGUID);
```

**PCM Doc:** 7.10 TpPos CRUD
```
TpPos tpPosReturn = await this.restService.TpPosUpdate(tpPosObject, MID, SessionGUID);
```

**PCM Doc:** 7.11.1 Wp GetItems
```
ArrayOfWp arrOfWp = await this.restService.WpGetItems(MID, SessionGUID);
```

**PCM Doc:** 7.11.2 Wp GetItembyID
```
ArrayOfWp arrOfWpByID = await this.restService.WpGetItemById(MID, 8, SessionGUID);
```

**PCM Doc:** 7.12.1 WpPos GetItemById
```
ArrayOfWpPos arrOfWpPos = await this.restService.WpPosGetItemById(MID, 8, SessionGUID);
```

**PCM Doc:** 7.13 WpPos CRUD
```
WpPos wpPosReturn = await this.restService.WpPosUpdate(wpPosObject, MID, SessionGUID);
```
